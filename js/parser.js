(function ($) {
  $(document).ready(function(){
    $('.parser-img-container').hide();
    $('.atc_loading').show();
    $('.atc_url').html($('.url').val());
    $('.parser-img-container').hide();
    //Flip Viewable Content
    $('.attach_content').fadeIn('slow');
    $('.atc_loading').hide();

    //Show first image
    $('.parser-img-container-1').fadeIn();
    $('.cur_image').val(1);
    $('.cur_image_num').html(1);

    // next image
    $('.next').unbind('click');
    $('.next').live("click", function(event){
      event.preventDefault();
      var thisParent = $(this).parent().parent();
      var total_images = parseInt($(thisParent).find('.atc_total_images').html());
      console.log(total_images);
      if (total_images > 0)
      {
        var index = $(thisParent).find('.cur_image').val();
        $(thisParent).find('.parser-img-container-' + index).hide();
        if(index < total_images)
        {
          new_index = parseInt(index) + parseInt(1);
        }
        else
        {
          new_index = 1;
        }

        $(thisParent).find('.cur_image').val(new_index);
        $(thisParent).find('.cur_image_num').html(new_index);
        $(thisParent).find('.parser-img-container-' + new_index).show();
      }
      return false;
    });

    // prev image
    $('.prev').unbind('click');
    $('.prev').live("click", function(event){
      event.preventDefault();
      var thisParent = $(this).parent().parent();
      var total_images = parseInt($(thisParent).find('.atc_total_images').html());
      if (total_images > 0)
      {
        var index = $(thisParent).find('.cur_image').val();
        $(thisParent).find('.parser-img-container-' + index).hide();
        if(index > 1)
        {
          new_index = parseInt(index) - parseInt(1);;
        }
        else
        {
          new_index = total_images;
        }

        $(thisParent).find('.cur_image').val(new_index);
        $(thisParent).find('.cur_image_num').html(new_index);
        $(thisParent).find('.parser-img-container-' + new_index).show();
      }
    });
  })
}) (jQuery);
