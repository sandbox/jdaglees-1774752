(function ($) {
  Drupal.behaviors.initParser = {
    attach: function (context){
      
      $('.parser-preview').click(function(){
        var domParent = $(this).parent();
        var delta = $(domParent).find('.parser-edit-form-delta').attr('value');
        var url = $(domParent).find('.parser-url').attr('value');
        $(domParent).find('.parser-loader').fadeIn();
        $.post(Drupal.settings.basePath + '?q=parser/get-data',
          {
          'url' : url,
          }
          ,function(data){
            $(domParent).find('.parser-loader').fadeOut();
            $(domParent).find('.parser-title').attr('value', data.title);
            $(domParent).find('.parser-body').attr('value', data.body);
            $(domParent).find('#parser-preview-'+delta).html(data.html);
          }
        );
        return false;

      });
    }
  }
})(jQuery);
