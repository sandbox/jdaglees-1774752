<?php
/**
 * @file
 * Main theme implementation.
 *
 * Variables available:
 * - $item: An array containing the current parsed link's data.
 * - $count: Total images count.
 */
global $base_url;

?>
<div class="attach_content" style="">
  <div class="atc-wrap"><input type='hidden' class='cur_image' name='cur_image' value='1'></input><div class='atc_images'> 
    <?php if( isset($item['urlImages']) ) {
      foreach($item['urlImages'] as $key => $image) {
        print ('<div class="parser-img-container parser-img-container-' 
        . ($key + 1) . '"><img src="' . check_url($image) . '" class="parser-img img-' 
        . ($key + 1) . '" width="100" style="display: inline;"/></div>');
      }
      $count = count($item['urlImages']);
    }else {
      print _parser_image_print($item['fid']);
      $count = substr_count($item['fid'], ',') + 1;
    }
    ?> 

</div></div>
  <div class="atc_info">
    <label class="atc_title"><?php print $item['title']; ?></label>
    <label class="atc_url"><?php print l($item['link'], $item['link']); ?></label>
    <br clear="all">
    <label class="atc_desc"><?php print $item['body']; ?></label>
    <br clear="all">
  </div>
  <div class="atc_total_image_nav">
    <?php print $item['navigation']; ?>
  </div>
  <div class="atc_total_images_info">
    Showing <span class="cur_image_num">1</span> of <span class="atc_total_images"><?php print $count; ?></span> images
  </div>
  <br clear="all">
</div>
