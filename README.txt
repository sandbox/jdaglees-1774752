ABOUT

Parser exposes a custom FieldAPI field type that stores parsed link data.

TODO

1. Create Form API field item to allow multiple images to be stored
2. Fetch images from external URL and save them into the database.

USAGE

Install the module at admin/modules. Create a new field in your entity (content
type, user, taxonomy term, etc) and choose Link Parser as the widget.
